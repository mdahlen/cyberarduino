#include "cyberparser.h"
#include "cyberping.h"
#include "cybermotor.h"

#define LEFT_MOTOR_ENABLE 9
#define LEFT_MOTOR_DIRECTION 4
#define LEFT_MOTOR_A_FEEDBACK 2
#define LEFT_MOTOR_B_FEEDBACK 7

#define RIGHT_MOTOR_ENABLE 10
#define RIGHT_MOTOR_DIRECTION 5
#define RIGHT_MOTOR_A_FEEDBACK 3
#define RIGHT_MOTOR_B_FEEDBACK 8

#define TRIGGERPIN 12
#define ECHOPIN 11


CyberMotor left(LEFT_MOTOR_ENABLE,LEFT_MOTOR_DIRECTION, LEFT_MOTOR_A_FEEDBACK, LEFT_MOTOR_B_FEEDBACK);
CyberMotor right(RIGHT_MOTOR_ENABLE, RIGHT_MOTOR_DIRECTION, RIGHT_MOTOR_A_FEEDBACK, RIGHT_MOTOR_B_FEEDBACK);

CyberPing ultra(TRIGGERPIN, ECHOPIN);

void setup() 
{
  Serial.begin(19200);

  left.reversed = -1;
  right.reversed = 1;

  // Class methods can't be attached to the interupt, need a "void x(void)" function as a wrapper
  attachInterrupt(0, leftEncoder, CHANGE);
  attachInterrupt(1, rightEncoder, CHANGE);
}

void loop() 
{
  checkSerial();
//  ultra.ping(50);

//  answer('R', 'F', 1, left.getRelativePosition());
//  answer('R', 'F', 2, right.getRelativePosition());
//  answer('R', 'F', 3, ultra.getDistance());

//  Serial.print(left.getRelativePosition());
//  Serial.print(' ');
//  Serial.println(right.getRelativePosition());
//  Serial.println(ultra.getDistance());
//  delay(100);

}

void leftEncoder(void)
{
  left.encoderCallback();
}

void rightEncoder(void)
{
  right.encoderCallback();
}





//  left.setSpeed(100);
//  Serial.println(left.speed);
//  delay(2000);
//  left.setSpeed(0);
//  Serial.println(left.speed);
//  delay(2000);
//  left.setSpeed(-100);
//  Serial.println(left.speed);
//  delay(2000);
//  left.setSpeed(0);
//  Serial.println(left.speed);
//  delay(2000);
//
//  right.setSpeed(100);
//  Serial.println(right.speed);
//  delay(2000);
//  right.setSpeed(0);
//  Serial.println(right.speed);
//  delay(2000);
//  right.setSpeed(-100);
//  Serial.println(right.speed);
//  delay(2000);
//  right.setSpeed(0);
//  Serial.println(right.speed);
//  delay(2000);
