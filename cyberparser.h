#include <Arduino.h>

#define RECBUFFER 128
#define SENDBUFFER 64

#define STARTMARKER '<'
#define ENDMARKER '>'
#define KEY_SEPARATOR ':'
#define FILTERCOMMAND "<: >"

void testParser(void);
void checkSerial(void);
void answer(char operation, char mode, byte pin, int value);
