#include <Arduino.h>

class CyberMotor
{
  public:
    int speed;
    int direction;
    byte enablePin;
    byte directionPin;
    char reversed;
    byte encoderPinA;
    byte encoderPinB;
    volatile int encoderPosition = 0;
    volatile int relativePosition = 0;

    void encoderCallback(void);
    CyberMotor(byte enablePin, byte directionPin, byte encoderPinA, byte encoderPinB): 
          enablePin(enablePin),
          directionPin(directionPin),
          encoderPinA(encoderPinA),
          encoderPinB(encoderPinB)
          { 
            pinMode(enablePin, OUTPUT);
            pinMode(directionPin, OUTPUT);
            pinMode(encoderPinA, INPUT);
            pinMode(encoderPinB, INPUT);
            digitalWrite(directionPin, 1);
          };
    void setSpeed(int sp);
    int getRelativePosition(void);
    
  private:
   
};
