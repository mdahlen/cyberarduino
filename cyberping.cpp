#include <Arduino.h>
#include "cyberping.h"

int CyberPing::ping(byte minDelay)
{
  if(millis() - lastScan >= minDelay)
  {
    digitalWrite(triggerPin, LOW);
    delayMicroseconds(2);
    digitalWrite(triggerPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(triggerPin, LOW);
    measurement[index] = pulseIn(echoPin, HIGH, 35000);
    if(index <= SCAN_PER_READING-1)
      index++;
    else
      index = 0;
    lastScan = millis();
  }
}

//  Remove smallest and largest value of the array, then divide the rest for a mean value.
int CyberPing::getDistance(void)
{
  int i;
  unsigned int smallest, largest;
  unsigned long mean = 0;

  largest = smallest = measurement[0];
  for(i = 0; i < SCAN_PER_READING; i++)
  {
    if(measurement[i] > largest)
      largest = measurement[i];
    else if(measurement[i] < smallest)
      smallest = measurement[i];
  }
  
  for(i = 0; i < SCAN_PER_READING; i++)
  {
    mean += measurement[i];
  }
  mean = mean - smallest - largest;

  return mean / 58 / (SCAN_PER_READING - 2);
}
