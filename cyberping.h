#include <Arduino.h>

#define SCAN_PER_READING 5

class CyberPing
{
  public:
    byte triggerPin;
    byte echoPin;
    unsigned long lastScan;
    unsigned int measurement[SCAN_PER_READING]; // Measurements in centimeter
    byte index = 0;

    CyberPing(byte triggerPin, byte echoPin): triggerPin(triggerPin), echoPin(echoPin) 
    {
      pinMode(triggerPin, OUTPUT);
      pinMode(echoPin, INPUT);
    };
    int getDistance();
    int ping(byte minDelay);
    
  private:
    
};

