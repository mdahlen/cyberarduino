#include <Arduino.h>
#include "cybermotor.h"

void CyberMotor::setSpeed(int sp)
{
  speed = constrain(sp, -255, 255);
  
  if(speed > 0)
  {
    digitalWrite(directionPin, 1);
    analogWrite(enablePin, speed);
  }
  else if(speed < 0)
  {
    digitalWrite(directionPin, 0);
    analogWrite(enablePin, (speed * -1));

  }
  else
    analogWrite(enablePin, 0);
  
}

void CyberMotor::encoderCallback(void)
{
  if(digitalRead(encoderPinA) == HIGH)
  {
    if(digitalRead(encoderPinB) == HIGH)
    {
      encoderPosition -= 1 * reversed;
    }
    else
    {
      encoderPosition += 1 * reversed;
    }
  }
  else
  {
    if(digitalRead(encoderPinB) == LOW)
    {
      encoderPosition -= 1 * reversed;
    }
    else
    {
      encoderPosition += 1 * reversed;
    }
  }
}

int CyberMotor::getRelativePosition(void)
{
  int temp = encoderPosition - relativePosition;
  relativePosition = encoderPosition;
  return temp;
}
