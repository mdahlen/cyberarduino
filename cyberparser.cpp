#include <Arduino.h>
#include "cyberparser.h"

volatile enum serialStatus {empty, incoming, newMessage} serialStatus; 
char recBuffer[RECBUFFER];
char sendBuffer[SENDBUFFER];
volatile byte recIndex;
volatile char recByte;


inline void answer(char operation, char mode, byte pin, int value)
{
  sprintf(sendBuffer, "%c%c%c%c%d%c%d%c", STARTMARKER, 
                                          operation, mode, 
                                          KEY_SEPARATOR, pin, 
                                          KEY_SEPARATOR, value, 
                                          ENDMARKER);
//  Serial.print(STARTMARKER);
//  Serial.print(operation);
//  Serial.print(mode);
//  Serial.print(KEY_SEPARATOR);
//  Serial.print(pin, DEC);
//  Serial.print(KEY_SEPARATOR);
//  Serial.print(value, DEC);
//  Serial.println(ENDMARKER);
  Serial.println(sendBuffer);
}

void set_pin_mode(byte pin, char mode)
{
    switch (mode){
        case 'I':
            pinMode(pin, INPUT);
            break;
        case 'O':
            pinMode(pin, OUTPUT);
            break;
        case 'P':
            pinMode(pin, INPUT_PULLUP);
            break;
    }
}

void digital_read(byte pin, byte value)
{
    value = digitalRead(pin);
    answer('R', 'D', pin, value);
}

void analog_read(byte pin, byte value)
{
    value = analogRead(pin);
    answer('R', 'A', pin, value);
}

void function_read(byte pin, byte value)
{
  
}




void digital_write(byte pin, byte value)
{
  digitalWrite(pin, value);
  //answer('W', 'D', pin, value);
}

void analog_write(byte pin, byte value)
{
  analogWrite(pin, value);
  answer('W', 'A', pin, value);

}

void function_write(byte pin, byte value)
{
  
}

void parseCommand(void)
{
  char operation;
  char mode;
  byte pin;
  byte value;
  
  char *command = strtok(recBuffer, FILTERCOMMAND);
  operation = command[0];
  mode = command[1];
  pin = atoi(strtok(NULL, FILTERCOMMAND));
  value = atoi(strtok(NULL, FILTERCOMMAND));

  if(operation == 'R')
  {
    if(mode == 'A')
      analog_read(pin, value);
    else if(mode == 'D')
      digital_read(pin, value);
    else if(mode == 'F')
      function_read(pin, value);
  }
  else if(operation == 'W')
  {
    if(mode == 'A')
    //{
      analog_write(pin, value);
    //} 
    else if(mode == 'D')
      digital_write(pin, value);
    else if(mode == 'F')
      function_write(pin, value);
  }
  else if(operation == 'M')
  {
    if(mode == 'I')
      set_pin_mode(pin, mode);
    else if(mode == 'O')
      set_pin_mode(pin, mode);
    else if(mode == 'P')
      set_pin_mode(pin, mode);
  }
}

void checkSerial(void)
{
  if(Serial.available() > 0)
  {
    recByte = Serial.read();
    if(recByte == STARTMARKER && serialStatus == empty)
    {
      recIndex = 0;
      recBuffer[0] = '\0';
      serialStatus = incoming;
    }

    recBuffer[recIndex++] = recByte;

    if(recByte == ENDMARKER && serialStatus == incoming)
    {
      recBuffer[recIndex] = '\0';
      serialStatus = newMessage;
//      serialStatus = empty;
//      Serial.print("-");
//      Serial.println(recBuffer);
    }
  }
  
  if(serialStatus == newMessage)
  {
    parseCommand();
    recIndex = 0;
    serialStatus = empty;
  }
}
